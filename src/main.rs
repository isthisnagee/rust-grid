extern crate cairo;
extern crate noise;
extern crate num;
extern crate rand;
extern crate time;

use std::fs::File;
use std::ops::Add;
use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd};
use num::Num;
use noise::{NoiseFn, Perlin, Seedable};
use rand::Rng;

use cairo::{Context, Format, ImageSurface};

fn main() {
    let now = time::now();
    let world = World {
        width: 60,
        height: 60,
        seed: now.tm_sec + now.tm_min + now.tm_hour,
        scale: 20,
    };

    let scaled_width = world.width * world.scale as i32;
    let scaled_height = world.height * world.scale as i32;

    let surface = ImageSurface::create(Format::ARgb32, scaled_width as i32, scaled_height as i32)
        .expect("Could not create image");
    let context = Context::new(&surface);
    context.rectangle(0., 0., scaled_width as f64, scaled_height as f64);
    context.set_source_rgb(239. / 255., 245. / 255., 213. / 255.);
    // context.set_source_rgb(71., 62., 90.);
    context.fill();

    let s_now = now.strftime("%d-%m-%y-%I-%M-%S").unwrap();
    let mut file = File::create(format!("output-{}.png", s_now)).expect("Couldn’t create file.");

    let grid = gen_quad_grid(world.width, world.height, world.scale as i32, world.seed);

    for quad in grid {
        render_closed_path(quad, &context);
    }

    surface
        .write_to_png(&mut file)
        .expect("Couldn’t write to png");

    println!("output-{}.png", s_now);
}

struct World {
    // using i because I want the grid origin to be at 0,0 and grow from there
    width: i32,
    height: i32,
    seed: i32,
    scale: u64,
}

// type ArtNum = PrimInt + Unsigned;
#[derive(Copy, Clone, Debug)]
struct Coord<T>
where
    T: Num,
{
    x: T,
    y: T,
}

impl Coord<i32> {
    pub fn to_f64(&self) -> Coord<f64> {
        return Coord {
            x: self.x as f64,
            y: self.y as f64,
        };
    }
}

impl Coord<f64> {
    pub fn to_array(&self) -> [f64; 3] {
        return [self.x + 0.13, self.y - 0.42, 1.2];
    }

    pub fn dot(self, other: Coord<f64>) -> f64 {
        self.x * other.x + self.y * other.y
    }

    // pub fn cross(self, other: Coord<f64>) -> Coord<f64> {
    //     Coord {
    //         x: self.x * other.y - self.y * other.x,
    //         y: self.y - other.y,
    //     }
    // }

    // pub fn from_array(a: [f64; 2]) -> Coord<f64> {
    //     return Coord { x: a[0], y: a[1] };
    // }
}

impl PartialEq for Quad<f64> {
    fn eq(&self, other: &Quad<f64>) -> bool {
        if (self.top_left.x > other.bottom_right.x) || (other.top_left.x > self.bottom_right.x) {
            return false;
        };
        if (self.top_left.y < other.bottom_right.y) || (other.top_left.x < self.bottom_right.y) {
            return false;
        };
        return true;
    }
}
impl Eq for Quad<f64> {}

impl PartialOrd for Quad<f64> {
    fn partial_cmp(&self, other: &Quad<f64>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Quad<f64> {
    fn cmp(&self, other: &Quad<f64>) -> Ordering {
        if self == other {
            return Ordering::Equal;
        };
        return Ordering::Greater;
    }
}

impl PartialOrd for Coord<f64> {
    fn partial_cmp(&self, other: &Coord<f64>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for Coord<f64> {
    fn cmp(&self, other: &Coord<f64>) -> Ordering {
        if self == other {
            return Ordering::Equal;
        };
        if (self.x < other.x) || (self.x == other.x && self.y < other.y) {
            return Ordering::Less;
        }
        return Ordering::Greater;
    }
}

impl PartialEq for Coord<f64> {
    fn eq(&self, other: &Coord<f64>) -> bool {
        return self.x == other.x && self.y == other.y;
    }
}

impl Eq for Coord<f64> {}

impl Add for Coord<f64> {
    type Output = Coord<f64>;

    fn add(self, other: Coord<f64>) -> Coord<f64> {
        Coord {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

#[derive(Debug)]
struct Quad<T>
where
    T: Num,
{
    top_left: Coord<T>,
    top_right: Coord<T>,
    bottom_left: Coord<T>,
    bottom_right: Coord<T>,
}

impl IntoIterator for Quad<f64> {
    type Item = Coord<f64>;
    type IntoIter = QuadIntoIterator;

    fn into_iter(self) -> Self::IntoIter {
        QuadIntoIterator {
            quad: self,
            index: 0,
        }
    }
}

struct QuadIntoIterator {
    quad: Quad<f64>,
    index: usize,
}

impl Iterator for QuadIntoIterator {
    type Item = Coord<f64>;
    fn next(&mut self) -> Option<Coord<f64>> {
        let result = match self.index {
            0 => self.quad.bottom_left,
            1 => self.quad.bottom_right,
            2 => self.quad.top_right,
            3 => self.quad.top_left,
            _ => return None,
        };
        self.index += 1;
        Some(result)
    }
}

impl QuadIntoIterator {
    // fn peek(&self) -> Option<Coord<f64>> {
    //     let result = match self.index {
    //         /*
    //          * When we're here, the index has already
    //          * been ++'d, so our coords are as follows
    //          *  4-----3
    //          * |     |
    //          * |     |
    //          * 1-----2
    //          */
    //         1 => self.quad.bottom_right,
    //         2 => self.quad.top_right,
    //         3 => self.quad.top_left,
    //         4 => self.quad.bottom_left,
    //         _ => return None,
    //     };
    //     Some(result)
    // }
}

impl Quad<f64> {
    pub fn first(&self) -> Coord<f64> {
        return self.top_left;
    }
    pub fn get_random(width: i32, height: i32, scale: i32) -> Quad<f64> {
        let mut rng = rand::thread_rng();
        let ref mut between = |v: i32| {
            rand::seq::sample_iter(&mut rng, 3..(v / 2 - 3), 1)
                .unwrap()
                .first()
                .unwrap()
                .clone()
        };

        let p: Coord<i32> = Coord {
            x: between(width) * scale * 2,
            y: between(height) * scale * 2,
        };
        let f_v = p.to_f64();
        let dist = 1.5 * scale as f64;
        return Quad {
            bottom_left: f_v,
            bottom_right: f_v + Coord { x: dist, y: 0. },
            top_right: f_v + Coord { x: dist, y: dist },
            top_left: f_v + Coord { x: 0., y: dist },
        };
    }

    pub fn add_noise(self, seed: u32) -> Quad<f64> {
        let mut perlin = Perlin::new();
        perlin = Seedable::set_seed(
            perlin,
            seed + (self.bottom_left.dot(self.bottom_left).sqrt()) as u32,
        );

        let p1 = perlin.get(self.bottom_left.to_array());
        let p2 = perlin.get(self.bottom_right.to_array());
        let p3 = perlin.get(self.top_left.to_array());
        let p4 = perlin.get(self.top_right.to_array());
        return Quad {
            bottom_left: self.bottom_left + Coord { x: p1, y: p1 },
            bottom_right: self.bottom_right + Coord { x: p2, y: p2 },
            top_left: self.top_left + Coord { x: p3, y: p3 },
            top_right: self.top_right + Coord { x: p4, y: p4 },
        };
    }
}

fn gen_quad_grid(width: i32, height: i32, scale: i32, seed: i32) -> Vec<Quad<f64>> {
    let mut grid: Vec<Quad<f64>> = (0..300)
        .map(|_| Quad::get_random(width, height, scale).add_noise(seed as u32))
        .collect();
    grid.sort();
    grid.dedup();
    return grid;
}

fn render_closed_path(quad: Quad<f64>, c: &Context) {
    let colors = [
        (215., 68., 80.),
        (79., 131., 223.),
        (28., 40., 38.),
        (45., 199., 149.),
        (219., 240., 180.),
    ];
    let mut rng = rand::thread_rng();
    let i: usize = rng.gen_range(0, 5);
    let color = colors[i];
    c.set_source_rgb(color.0 / 255., color.1 / 255., color.2 / 255.);

    let first = quad.first().clone();
    c.move_to(first.x, first.y);

    let mut iter = quad.into_iter();
    while let Some(coord) = iter.next() {
        c.line_to(coord.x, coord.y);
    }

    let fill: bool = rng.gen();
    if fill {
        c.fill();
    } else {
        c.stroke();
    }
}
